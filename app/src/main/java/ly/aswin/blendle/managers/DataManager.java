package ly.aswin.blendle.managers;

import java.util.List;

import ly.aswin.blendle.data.models.Article;
import ly.aswin.blendle.network.NetworkHandler;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Aswin on 23-3-2016
 */
public class DataManager {

    private static final int MAX_ADS = 6;
    private static final String AD_TITLE = "ADVERTISEMENT";

    public DataManager() {
    }

    public Subscription getPopularArticlesWithRandomAds(Subscriber<List<Article>> subscriber) {
        return NetworkHandler.getInstance().getBlendleService().getPopularArticles()
                .map(new Func1<List<Article>, List<Article>>() {
                    @Override
                    public List<Article> call(List<Article> articles) {
                        addRandomAdsToArticles(articles);
                        return articles;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(subscriber);
    }

    private void addRandomAdsToArticles(List<Article> articles) {
        if (articles != null && !articles.isEmpty()) {
            int adOffset = articles.size() / MAX_ADS;
            int adPosition = adOffset;
            for (int i = 0; i < MAX_ADS; i++) {
                articles.add(adPosition, createAdArticle());
                adPosition += adOffset;
            }
        }
    }

    private Article createAdArticle() {
        Article adArticle = new Article();
        adArticle.setAd(true);
        adArticle.setTitle(AD_TITLE);
        return adArticle;
    }
}
