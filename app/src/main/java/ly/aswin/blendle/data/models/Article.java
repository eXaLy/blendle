package ly.aswin.blendle.data.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import ly.aswin.blendle.BR;

/**
 * Created by Aswin on 23-3-2016
 */
public class Article extends BaseObservable implements Parcelable {

    @Bindable
    private String id;

    @Bindable
    private String date;

    @Bindable
    private String imageUrl;

    @Bindable
    private String kicker;

    @Bindable
    private String title;

    @Bindable
    private String intro;

    @Bindable
    private String subtitle;

    @Bindable
    private String byline;

    @Bindable
    private List<String> paragraphs;

    @Bindable
    private boolean isAd;

    private boolean isExpanded;

    public Article() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
        notifyPropertyChanged(BR.date);
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        notifyPropertyChanged(BR.imageUrl);
    }

    public String getKicker() {
        return kicker;
    }

    public void setKicker(String kicker) {
        this.kicker = kicker;
        notifyPropertyChanged(BR.kicker);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
        notifyPropertyChanged(BR.intro);
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
        notifyPropertyChanged(BR.subtitle);
    }

    public String getByline() {
        return byline;
    }

    public void setByline(String byline) {
        this.byline = byline;
        notifyPropertyChanged(BR.byline);
    }

    public List<String> getParagraphs() {
        return paragraphs;
    }

    public void setParagraphs(List<String> paragraphs) {
        this.paragraphs = paragraphs;
        notifyPropertyChanged(BR.paragraphs);
    }

    public boolean isAd() {
        return isAd;
    }

    public void setAd(boolean ad) {
        isAd = ad;
    }

    @Bindable
    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
        notifyPropertyChanged(BR.expanded);
    }

    @Bindable
    public String getHtmlBody() {
        String body = "";
        if (title != null) {
            body += "<h1>" + title + "</h1><";
        }
        if (subtitle != null) {
            body += "<h4>" + subtitle + "</h4>";
        }
        if (byline != null) {
            body += "<p><i>" + byline + "</i></p>";
        }
        if (intro != null) {
            body += "<p>" + intro + "</p>";
        }
        if (paragraphs != null && !paragraphs.isEmpty()) {
            for (String p : paragraphs) {
                body += "<p>" + p + "</p>";
            }
        }
        return body;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.date);
        dest.writeString(this.imageUrl);
        dest.writeString(this.kicker);
        dest.writeString(this.title);
        dest.writeString(this.intro);
        dest.writeString(this.subtitle);
        dest.writeString(this.byline);
        dest.writeStringList(this.paragraphs);
    }

    protected Article(Parcel in) {
        this.id = in.readString();
        this.date = in.readString();
        this.imageUrl = in.readString();
        this.kicker = in.readString();
        this.title = in.readString();
        this.intro = in.readString();
        this.subtitle = in.readString();
        this.byline = in.readString();
        this.paragraphs = in.createStringArrayList();
    }

    public static final Parcelable.Creator<Article> CREATOR = new Parcelable.Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel source) {
            return new Article(source);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };
}
