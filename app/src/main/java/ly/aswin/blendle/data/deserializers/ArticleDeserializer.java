package ly.aswin.blendle.data.deserializers;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ly.aswin.blendle.data.models.Article;

/**
 * Created by Aswin on 23-3-2016
 */
public class ArticleDeserializer implements JsonDeserializer<Article> {

    private static final String KEY_EMBEDDED = "_embedded";
    private static final String KEY_LINKS = "_links";
    private static final String KEY_ORIGINAL = "original";
    private static final String KEY_HREF = "href";
    private static final String KEY_MANIFEST = "manifest";
    private static final String KEY_ID = "id";
    private static final String KEY_DATE = "date";
    private static final String KEY_BODY = "body";
    private static final String KEY_IMAGES = "images";
    private static final String KEY_TYPE = "type";
    private static final String KEY_CONTENT = "content";

    private static final String TYPE_KICKER = "kicker";
    private static final String TYPE_INTRO = "intro";
    private static final String TYPE_HL1 = "hl1";
    private static final String TYPE_HL2 = "hl2";
    private static final String TYPE_BYLINE = "byline";
    private static final String TYPE_P = "p";

    @Override
    public Article deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject data = json.getAsJsonObject();
        if (data != null && !data.isJsonNull()) {
            JsonObject obj = data.get(KEY_EMBEDDED).getAsJsonObject().get(KEY_MANIFEST).getAsJsonObject();
            Article article = new Article();
            article.setId(obj.get(KEY_ID).getAsString());
            article.setDate(obj.get(KEY_DATE).getAsString());
            setBodyData(article, obj);
            setImage(article, obj);
            return article;
        }
        return null;
    }

    private void setBodyData(Article article, JsonObject data) {
        JsonArray bodyArray = data.get(KEY_BODY).getAsJsonArray();
        if (bodyArray != null && bodyArray.size() > 0) {
            for (int i = 0, len = bodyArray.size(); i < len; i++) {
                JsonObject obj = bodyArray.get(i).getAsJsonObject();
                String type = obj.get(KEY_TYPE).getAsString();
                String content = obj.get(KEY_CONTENT).getAsString();
                setTypedData(article, type, content);
            }
        }
    }

    private void setTypedData(Article article, String type, String content) {
        switch (type) {
            case TYPE_KICKER:
                article.setKicker(content);
                break;
            case TYPE_BYLINE:
                article.setByline(content);
                break;
            case TYPE_HL1:
                article.setTitle(content);
                break;
            case TYPE_HL2:
                article.setSubtitle(content);
                break;
            case TYPE_INTRO:
                article.setIntro(content);
                break;
            case TYPE_P:
                List<String> paragraphs = article.getParagraphs();
                if (paragraphs == null) {
                    paragraphs = new ArrayList<>();
                }
                paragraphs.add(content);
                article.setParagraphs(paragraphs);
                break;
        }
    }

    private void setImage(Article article, JsonObject data) {
        JsonArray images = data.get(KEY_IMAGES).getAsJsonArray();
        if (images != null && images.size() > 0) {
            JsonObject imageObj = images.get(0).getAsJsonObject();
            String imgUrl = imageObj.get(KEY_LINKS).getAsJsonObject().get(KEY_ORIGINAL)
                    .getAsJsonObject().get(KEY_HREF).getAsString();
            article.setImageUrl(imgUrl);
        }
    }
}
