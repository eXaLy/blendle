package ly.aswin.blendle.data.deserializers;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ly.aswin.blendle.data.models.Article;

/**
 * Created by Aswin on 23-3-2016
 */
public class PopularArticlesDeserializer implements JsonDeserializer<List<Article>> {

    private static final String KEY_EMBEDDED = "_embedded";
    private static final String KEY_ISSUES = "issues";

    @Override
    public List<Article> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray data = json.getAsJsonObject().get(KEY_EMBEDDED).getAsJsonObject().get(KEY_ISSUES).getAsJsonArray();
        List<Article> articles = new ArrayList<>();
        if (data != null && data.size() > 0) {
            for (int i = 0, len = data.size(); i < len; i++) {
                JsonElement element = data.get(i);
                articles.add((Article) context.deserialize(element, Article.class));
            }
        }
        return articles;
    }
}
