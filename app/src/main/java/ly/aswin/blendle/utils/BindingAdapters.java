package ly.aswin.blendle.utils;

import android.databinding.BindingAdapter;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Aswin on 24-3-2016
 */
public class BindingAdapters {
    @BindingAdapter("bind:imageUrl")
    public static void loadImage(ImageView imageView, String url) {
        if (url != null && !url.isEmpty()) {
            Picasso.with(imageView.getContext()).load(url).into(imageView);
        }
    }

    @BindingAdapter("bind:htmlText")
    public static void setHtmlText(TextView textView, String htmlText) {
        if (htmlText != null) {
            textView.setText(Html.fromHtml(htmlText));
        }
    }
}