package ly.aswin.blendle.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by Aswin on 26-3-2016
 */
public class Utils {

    private static final String URL_AD_WEBSITE = "https://aswin.ly";

    public static void openAd(Context context) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(URL_AD_WEBSITE));
        context.startActivity(i);
    }
}
