package ly.aswin.blendle.network;

/**
 * Created by Aswin on 23-3-2016
 */
public class WSConstants {
    public static final String BASE_URL = "https://static.blendle.nl/";
    public static final String POPULAR_ENDPOINT = "meta/newsstand/location/ISO-3166-1/code/NL/popular.json";
}
