package ly.aswin.blendle.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.List;
import java.util.concurrent.TimeUnit;

import ly.aswin.blendle.BuildConfig;
import ly.aswin.blendle.data.deserializers.ArticleDeserializer;
import ly.aswin.blendle.data.deserializers.PopularArticlesDeserializer;
import ly.aswin.blendle.data.models.Article;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Aswin on 23-3-2016
 */
public class NetworkHandler {
    /*************************************
     * VARIABLES
     *************************************/

    private static NetworkHandler networkHandler;

    private BlendleService blendleService;

    /*************************************
     * SINGLETON
     *************************************/

    public static NetworkHandler getInstance() {
        if (networkHandler == null) {
            networkHandler = new NetworkHandler();
        }
        return networkHandler;
    }

    public NetworkHandler() {

    }

    /*************************************
     * PUBLIC METHODS
     *************************************/

    public BlendleService getBlendleService() {
        if (blendleService == null) {
            initBlendleService();
        }
        return blendleService;
    }

    /*************************************
     * PRIVATE METHODS
     *************************************/

    private void initBlendleService() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(createLoggingInterceptor())
                .readTimeout(1, TimeUnit.MINUTES)
                .connectTimeout(1, TimeUnit.MINUTES)
                .build();

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(new TypeToken<List<Article>>(){}.getType(),
                        new PopularArticlesDeserializer())
                .registerTypeAdapter(Article.class, new ArticleDeserializer())
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WSConstants.BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        this.blendleService = retrofit.create(BlendleService.class);
    }

    private HttpLoggingInterceptor createLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }
        return interceptor;
    }
}
