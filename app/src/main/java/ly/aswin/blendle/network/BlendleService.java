package ly.aswin.blendle.network;

import java.util.List;

import ly.aswin.blendle.data.models.Article;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Aswin on 23-3-2016
 */
public interface BlendleService {
    @GET(WSConstants.POPULAR_ENDPOINT)
    Observable<List<Article>> getPopularArticles();
}
