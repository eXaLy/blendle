package ly.aswin.blendle.ui.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ly.aswin.blendle.data.models.Article;
import ly.aswin.blendle.databinding.CardArticleItemBinding;

public class ArticlesRecyclerViewAdapter extends RecyclerView.Adapter<ArticlesRecyclerViewAdapter.ViewHolder> {

    private final List<Article> articles;
    private OnArticleClickListener onArticleClickListener;

    public ArticlesRecyclerViewAdapter() {
        this.articles = new ArrayList<>();
    }

    public void addArticles(List<Article> articles) {
        this.articles.addAll(articles);
        notifyItemRangeInserted(articles.size() - articles.size(), articles.size());
    }

    public void clearArticles() {
        articles.clear();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardArticleItemBinding binding = CardArticleItemBinding.inflate(LayoutInflater
                .from(parent.getContext()), parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Article article = articles.get(position);
        holder.binding.setArticle(article);
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public void setOnArticleClickListener(OnArticleClickListener onArticleClickListener) {
        this.onArticleClickListener = onArticleClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CardArticleItemBinding binding;

        public ViewHolder(View view) {
            super(view);
            this.binding = DataBindingUtil.bind(view);
            binding.articleCardView.setReadMoreClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onArticleClickListener != null) {
                Article article = articles.get(getAdapterPosition());
                onArticleClickListener.onArticleClick(article, binding.articleCardView.getThumbnail());
            }
        }
    }

    public interface OnArticleClickListener {
        void onArticleClick(Article article, View sharedImage);
    }
}
