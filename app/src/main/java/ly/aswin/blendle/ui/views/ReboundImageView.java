package ly.aswin.blendle.ui.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringListener;
import com.facebook.rebound.SpringSystem;

/**
 * Created by Aswin on 25-4-2016
 */
public class ReboundImageView extends ImageView {

    private OnClickListener onImageClickListener;

    private static final double TENSION = 400;
    private static final double DAMPER = 20;
    private static final float UNSELECTED = 0f;
    private static final float SELECTED = 1f;
    private static final float SCALE = 0.2f;

    public ReboundImageView(Context context) {
        super(context);
        init();
    }

    public ReboundImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ReboundImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ReboundImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void setOnImageClickListener(OnClickListener onImageClickListener) {
        this.onImageClickListener = onImageClickListener;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void init() {
        final Spring spring = SpringSystem.create().createSpring();
        spring.setSpringConfig(new SpringConfig(TENSION, DAMPER));
        spring.addListener(new SpringListener() {
            @Override
            public void onSpringUpdate(Spring spring) {
                float val = (float) spring.getCurrentValue();
                float scale = SELECTED - (val * SCALE);
                ReboundImageView.this.setScaleX(scale);
                ReboundImageView.this.setScaleY(scale);
            }

            @Override
            public void onSpringAtRest(Spring spring) {

            }

            @Override
            public void onSpringActivate(Spring spring) {

            }

            @Override
            public void onSpringEndStateChange(Spring spring) {

            }
        });

        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        spring.setEndValue(SELECTED);
                        return true;
                    case MotionEvent.ACTION_UP:
                        if (onImageClickListener != null) {
                            onImageClickListener.onClick(ReboundImageView.this);
                        }
                        spring.setEndValue(UNSELECTED);
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        spring.setEndValue(UNSELECTED);
                        return true;
                }

                return false;
            }
        });
    }
}
