package ly.aswin.blendle.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import ly.aswin.blendle.R;
import ly.aswin.blendle.data.models.Article;
import ly.aswin.blendle.managers.DataManager;
import ly.aswin.blendle.ui.adapters.ArticlesRecyclerViewAdapter;
import ly.aswin.blendle.utils.Utils;
import rx.Subscriber;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String KEY_SHARED_IMAGE = "articleImage";

    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private DataManager dataManager;
    private ArticlesRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initData();
    }

    private void initViews() {
        this.progressBar = (ProgressBar) findViewById(R.id.progressBar);
        this.swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        this.adapter = new ArticlesRecyclerViewAdapter();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadArticles();
            }
        });
        adapter.setOnArticleClickListener(new ArticlesRecyclerViewAdapter.OnArticleClickListener() {
            @Override
            public void onArticleClick(Article article, View sharedImage) {
                if (article.isAd()) {
                    Utils.openAd(MainActivity.this);
                } else {
                    openArticle(article, sharedImage);
                }
            }
        });
    }

    private void initData() {
        this.dataManager = new DataManager();
        progressBar.setVisibility(View.VISIBLE);
        loadArticles();
    }

    private void loadArticles() {
        dataManager.getPopularArticlesWithRandomAds(new Subscriber<List<Article>>() {
            @Override
            public void onCompleted() {
                hideLoadingIndicators();
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "Loading articles went wrong", e);
            }

            @Override
            public void onNext(List<Article> articles) {
                adapter.clearArticles();
                adapter.addArticles(articles);
            }
        });
    }

    private void openArticle(Article article, View sharedImage) {
        Intent intent = new Intent(this, ArticleActivity.class);
        intent.putExtra(ArticleActivity.KEY_ARTICLE, article);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                    sharedImage, KEY_SHARED_IMAGE);
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

    private void hideLoadingIndicators() {
        progressBar.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
    }
}
