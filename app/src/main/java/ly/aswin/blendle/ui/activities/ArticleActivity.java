package ly.aswin.blendle.ui.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import ly.aswin.blendle.R;
import ly.aswin.blendle.data.models.Article;
import ly.aswin.blendle.databinding.ActivityArticleBinding;
import ly.aswin.blendle.utils.Utils;

public class ArticleActivity extends AppCompatActivity {

    public static final String KEY_ARTICLE = "KEY_ARTICLE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLayout();
        initToolbar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initLayout() {
        ActivityArticleBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_article);
        Article article = getIntent().getParcelableExtra(KEY_ARTICLE);
        binding.setArticle(article);
        binding.articleAdview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.openAd(ArticleActivity.this);
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
