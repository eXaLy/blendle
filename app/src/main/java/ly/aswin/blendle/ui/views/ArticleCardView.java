package ly.aswin.blendle.ui.views;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import ly.aswin.blendle.data.models.Article;
import ly.aswin.blendle.databinding.ViewArticleCardBinding;

/**
 * Created by Aswin on 24-4-2016
 */
public class ArticleCardView extends CardView {

    private ViewArticleCardBinding binding;
    private OnClickListener toggleCollapseViewOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Article article = binding.getArticle();
            article.setExpanded(!article.isExpanded());
        }
    };

    public ArticleCardView(Context context) {
        super(context);
        init();
    }

    public ArticleCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ArticleCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setArticle(Article article) {
        binding.setArticle(article);
    }

    public ImageView getThumbnail() {
        return binding.articleCardThumbnail;
    }

    public void setReadMoreClickListener(OnClickListener listener) {
        binding.articleCardReadMore.setOnClickListener(listener);
    }

    private void init() {
        this.binding = ViewArticleCardBinding.inflate(LayoutInflater.from(getContext()), this, true);
        binding.articleCardThumbnail.setOnImageClickListener(toggleCollapseViewOnClickListener);
        setOnClickListener(toggleCollapseViewOnClickListener);
    }
}
