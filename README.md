# Blendle Demo App

## How to install
In the root folder you can find the file `app-release-1.1.1.apk`. Use this file to install on your Android device.
Android 5.0 (Lollipop) or higher recommended for the best experience.

## How to build
Go to the root folder and run the command:
```sh
$ gradlew assemble
```
You can find the builds (release and debug) in the directory `[root]/app/build/outputs/apk`.
For demo purposes, the release build key is included in the root folder: `blendle_demo.jks`.
